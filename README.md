# Readme.md
### Insert some appropriate content to describe the purpose of this specific repository

---

## Requirements

## Role Variables

## Dependencies

## Example Usage

## License

### Level 3

To use it, simply:

* Type Markdown text in the left pane
* See the HTML in the right

**WriteMe.md** also supports GitHub-style syntax highlighting for numerous languages, like so:

```html
<h1>Enjoy</h1>
```

```css
h1:after {
  content: 'using';
}
```

```js
console.log('WriteMe.md');
```

---

To learn the basics of using Markdown, **[read this](http://daringfireball.net/projects/markdown/basics)**.
